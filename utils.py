import hashlib
import hmac
from datetime import datetime
from urllib import parse

import requests
import ujson


class Utils:
    def __init__(self, config: dict) -> None:
        self._conf = config

    @staticmethod
    def get_headers_with_signature(
        domain: str,
        method: str,
        url: str,
        query_string: str,
        headers: dict,
        body="",
        exclude_headers=[],
        sign_query=False,
    ) -> dict:
        """Returns headers with signature."""
        query_string = parse.unquote(query_string)
        fp_date = datetime.now().strftime("%Y%m%dT%H%M%SZ")
        headers_str = ""
        host = domain.replace("https://", "").replace("http://", "")
        headers["host"] = host

        if not sign_query:
            headers["x-fp-date"] = fp_date
        else:
            query_string += (
                f"&x-fp-date={fp_date}" if query_string else f"?x-fp-date={fp_date}"
            )

        excluded_headers = {}
        for header in exclude_headers:
            excluded_headers[header] = (
                headers.pop(header) if header in headers else None
            )
        for key, val in headers.items():
            headers_str += f"{key}:{val}\n"

        body_hex = hashlib.sha256("".encode()).hexdigest()
        if body:
            body_hex = hashlib.sha256(
                ujson.dumps(body).replace(", ", ",").replace(": ", ":").encode()
            ).hexdigest()

        request_list = [
            method.upper(),
            url,
            query_string,
            headers_str,
            ";".join(
                [h for h in headers.keys() if h == "host" or h.startswith("x-fp-")]
            ),
            body_hex,
        ]

        request_str = "\n".join(request_list)
        request_str = "\n".join(
            [fp_date, hashlib.sha256(request_str.encode()).hexdigest()]
        )
        signature = (
            "v1.1:"
            + hmac.new(
                "1234567".encode(), request_str.encode(), hashlib.sha256
            ).hexdigest()
        )

        if not sign_query:
            headers["x-fp-signature"] = signature
        else:
            query_string += f"&x-fp-signature={signature}"

        for h_key, h_value in excluded_headers.items():
            if h_value:
                headers[h_key] = h_value

        return headers if not sign_query else query_string

    @staticmethod
    def get_access_token():
        url = "https://api.fyndx1.de/service/panel/authentication/v1.0/company/1/oauth/staff/token"
        headers = {
            "authority": "api.fyndx1.de",
            "accept": "application/json, text/plain, */*",
            "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
            "cookie": "_fbp=fb.1.1654843687710.95082261; _ga=GA1.1.431104896.1654843688; hubspotutk=35bf8901286d8736080ab81719d79d13; __hssrc=1; __hstc=207684365.35bf8901286d8736080ab81719d79d13.1654843687972.1656306907531.1656396988675.13; x.session=s:pyv_ibncTFKRQ2KqVz2xnnkeFxGJMgyq.4hpZGKWH0Ob66YHfdYQy6mS7f3qtbx3AmXpO5hK3Eis; _ga_0GX7SFJN3X=GS1.1.1656396988.21.1.1656397005.0; __hssc=207684365.2.1656396988675",
            "origin": "https://platform.fyndx1.de",
            "referer": "https://platform.fyndx1.de/",
            "sec-ch-ua": '" Not A;Brand";v="99", "Chromium";v="102", "Google Chrome";v="102"',
            "sec-ch-ua-mobile": "?0",
            "sec-ch-ua-platform": '"macOS"',
            "sec-fetch-dest": "empty",
            "sec-fetch-mode": "cors",
            "sec-fetch-site": "same-site",
            "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36",
            "x-fp-date": "20220607T133105Z",
            "x-fp-signature": "v1.1:2e1c02d546727d48fd5ebeea6aaa800eb0c9d170eb924c045da02b65f3fedf3e",
        }
        response = requests.request("GET", url, headers=headers)
        json_response = response.text
        data = ujson.loads(json_response)
        return data["access_token"]


if __name__ == "__main__":
    print(Utils.get_access_token())
