import json
from fdk_client.common.utils import (
    get_headers_with_signature,
    create_url_without_domain,
    create_query_string,
)
from fdk_client.common.aiohttp_helper import AiohttpHelper
from utils import Utils

payload = {
    "payload": [
        {
            "seller_identifier": "NEWSIZE5JUN22",
            "store_id": 12144,
            "total_quantity": 900,
            "price_effective": 800,
            "price_marked": 3000,
        },
        {
            "seller_identifier": "NEWSIZE5JUN22",
            "store_id": 12144,
            "total_quantity": 900,
            "price_effective": 800,
            "price_marked": 3000,
        },
        {
            "seller_identifier": "NEWSIZE5JUN22",
            "store_id": 12144,
            "total_quantity": 900,
            "price_effective": 800,
            "price_marked": 3000,
        },
    ],
    "meta": {"action": "upsert", "trace_id": "629cc33ebb33960001622f75"},
    "company_id": 1,
    "tracking_id": "629cc33ebb33960001622f75",
}


headers = {
    "x-user-data": json.dumps(
        {"username": "nikhilmhatre_gofynd_com_85097", "_id": 35, "uid": 35}
    ),
    "cookie": """x.session=s:y9hhboWqY4m7KxVoJP4b3wOwPXH9Vf55.sVGUl6RjgcbIOCZTHHIMVa8JBbva0YFSSkPvinyeWok""",
    "authorization": "Bearer " + Utils.get_access_token(),
}

import asyncio


async def main():
    url_without_domain = await create_url_without_domain(
        "/service/platform/catalog/v2.0/company/1/inventory",
    )
    query_string = await create_query_string()
    exclude_headers = []
    for key, val in headers.items():
        if not key.startswith("x-fp-"):
            exclude_headers.append(key)

    h = await get_headers_with_signature(
        domain="https://api.fyndx1.de",
        method="post",
        url=url_without_domain,
        query_string=query_string,
        headers=headers,
        body=payload,
        exclude_headers=exclude_headers,
        #sign_query=True,
    )
    resp = await AiohttpHelper().aiohttp_request(
        "POST",
        "https://api.fyndx1.de/service/platform/catalog/v2.0/company/1/inventory",
        headers=headers,
        data=payload,
    )
    #print(resp)
    print(resp['json'])



asyncio.get_event_loop().run_until_complete(main())


